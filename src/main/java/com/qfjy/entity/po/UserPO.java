package com.qfjy.entity.po;

import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName UserPO
 * @Description TODO
 * @Author guoweixin
 * @Date 2021/8/6
 * @Version 1.0
 */
@Data
public class UserPO implements Serializable {
    private String id;
    private String name;
    private int age;


    public static String keyName(){
        return "userpo";
    }
}
