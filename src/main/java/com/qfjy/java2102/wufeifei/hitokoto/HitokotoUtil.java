package com.qfjy.java2102.wufeifei.hitokoto;

import com.qfjy.java2102.wufeifei.pojo.Hitokoto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * Hitokoto
 */
@Component
public class HitokotoUtil {
    @Autowired
    private RestTemplate restTemplate;

    private String url="https://v1.hitokoto.cn/?c=g";
    /**
     * RestTemplate ；通过java 代码完成服务间通信访问
     * param1:请求URL
     * param2:请求后的返回值
     * param3:请求的参数对象
     */
    public  String  getOneWord() throws Exception{
        Hitokoto hitokoto=restTemplate.getForObject(url, Hitokoto.class);
        String str = hitokoto.getHitokoto();
       return str;
    }

}
