package com.qfjy.java2102.wufeifei.common;

public enum ResponseCodeEnum {
    SUCCESS("200","登录成功"),
    FAILURE("500","请输入正确的账号密码"),
    FAILURE_NULL("500","输入框内容不能为空"),
    FAILURE_CODE("500","验证码有误"),
    FAILURE_PASSWORD("500","密码有误"),
    FAILURE_PASSWORD_5TIMES("500","密码错误五次，一小时后请重试"),
    FAILURECOUNT("515","密码五次错误，20分钟后重新登录"),
    FAILUREUSERNAME("516","账号错误"),
    FAILUREPASSWORD("517","密码错误");
    private String code;
    private String msg;

    ResponseCodeEnum(String code, String msg) {

        this.code = code;
        this.msg = msg;
    }
    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
