package com.qfjy.java2102.wufeifei.common;

import lombok.Data;

@Data
public class DataResult {
    private String code;
    private String msg;
    private Object data;
    public DataResult() {
    }

    public DataResult(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public DataResult(String code, String msg, Object obj) {
        this.code = code;
        this.msg = msg;
        this.data = obj;
    }
    public static DataResult success() {
        return new DataResult(ResponseCodeEnum.SUCCESS.getCode(), ResponseCodeEnum.SUCCESS.getMsg());
    }

    public static DataResult success(Object obj) {
        return new DataResult(ResponseCodeEnum.SUCCESS.getCode(), ResponseCodeEnum.SUCCESS.getMsg(), obj);
    }

    public static DataResult failure() {
        return new DataResult(ResponseCodeEnum.FAILURE.getCode(), ResponseCodeEnum.FAILURE.getMsg());
    }
    public static DataResult failureCount() {
        return new DataResult(ResponseCodeEnum.FAILURECOUNT.getCode(), ResponseCodeEnum.FAILURECOUNT.getMsg());
    }
    public static DataResult failureUsername() {
        return new DataResult(ResponseCodeEnum.FAILUREUSERNAME.getCode(), ResponseCodeEnum.FAILUREUSERNAME.getMsg());
    }
    public static DataResult failureNull() {
        return new DataResult(ResponseCodeEnum.FAILURE_NULL.getCode(), ResponseCodeEnum.FAILURE_NULL.getMsg());
    }
    public static DataResult failureCode() {
        return new DataResult(ResponseCodeEnum.FAILURE_CODE.getCode(), ResponseCodeEnum.FAILURE_CODE.getMsg());
    }
    public static DataResult failurePassword() {
        return new DataResult(ResponseCodeEnum.FAILURE_PASSWORD.getCode(), ResponseCodeEnum.FAILURE_PASSWORD.getMsg());
    }
    public static DataResult failurePassword5Times() {
        return new DataResult(ResponseCodeEnum.FAILURE_PASSWORD_5TIMES.getCode(), ResponseCodeEnum.FAILURE_PASSWORD_5TIMES.getMsg());
    }

}