package com.qfjy.java2102.wufeifei.pojo;

import lombok.Data;

import java.io.Serializable;

@Data
public class User implements Serializable {
    private String phone;
    private String code;
    private String password;
}
