package com.qfjy.java2102.wufeifei.pojo;

import lombok.Data;

import java.io.Serializable;

@Data
public class Hitokoto implements Serializable {
    private String id;
    private String uuid;
    private String hitokoto;
    private String type;
    private String from_who;
    private String creator;
    private String creator_uid;
    private String reviewer;
    private String commit_from;
    private String created_at;
    private String length;


}