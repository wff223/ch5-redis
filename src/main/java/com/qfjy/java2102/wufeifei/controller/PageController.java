package com.qfjy.java2102.wufeifei.controller;

import com.qfjy.java2102.wufeifei.common.DataResult;
import com.qfjy.java2102.wufeifei.hitokoto.HitokotoUtil;
import com.qfjy.java2102.wufeifei.pojo.Hitokoto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@CrossOrigin
@Controller
@RequestMapping("to")
public class PageController {

    @Autowired
    private  RedisTemplate<String,Object> redisTemplate;
    @Autowired
    private HitokotoUtil hitokotoUtil;

    @RequestMapping("login")
    public String toLogin(HttpServletRequest request){
        //从一言工具类中获得一句话存入request

        String oneWord = null;
        try {
            oneWord = hitokotoUtil.getOneWord();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
       request.setAttribute("oneWord",oneWord);

        return "login";
    }
    @RequestMapping("index")
    public String toIndex(){
        return "index";
    }
    @RequestMapping("code")
    @ResponseBody
    public DataResult toCode(@RequestParam("phone") String phone, HttpServletRequest request){
        DataResult dataResult = new DataResult();
        String ip = request.getRemoteAddr();



        HashMap<String, String> stringHashMap = new HashMap<String, String>();
        int code = 0;
        int count =0;
        //判断是否第一次获取验证码

        if (!redisTemplate.hasKey("user:"+phone)){

            Random random = new Random();
            //生成1000-10000内的随机数
            code = random.nextInt(8999)+1000;
            count+=1;
            stringHashMap.put(String.valueOf(code),String.valueOf(count));

            //写入数据库中
            BoundHashOperations<String, Object, Object> user = redisTemplate.boundHashOps("user:"+phone);
            user.put(ip,stringHashMap);
            // redisTemplate.opsForSet().add(ip,stringHashMap);
            //resource.hset(ip,stringHashMap);
            //设定有效时长

            redisTemplate.expire("user:"+phone,60,TimeUnit.SECONDS);

            Long expire = redisTemplate.getExpire("user:"+phone);
            //向手机号码发送验证码
            System.out.println("第"+count+"次请求，验证码为："+code+"剩余时间"+expire);

            dataResult.setCode("200");

        }else {
            //获取ip对应的code和count
            HashMap<String, String>  map = (HashMap<String, String>) redisTemplate.boundHashOps("user:"+phone).get(ip);
           // HashMap<String, String>  map = (HashMap<String, String>) redisTemplate.opsForHash().get(ip,stringHashMap);
           // HashMap<String, String> map= (HashMap<String, String>) resource.hgetAll(ip);

            for(Map.Entry<String, String> entry : map.entrySet()){
                code=Integer.parseInt(entry.getKey());
                count=Integer.parseInt( entry.getValue());
            }

            //判断次数
            //在2-3次之间获取
            if(count<3){
                //count+1，再次向手机号码发送短信
                count+=1;
                Long expire = redisTemplate.getExpire("user:"+phone);
                System.out.println("第"+count+"次请求，验证码为："+code+"剩余时间"+expire);
                map.put(String.valueOf(code),String.valueOf(count));
                BoundHashOperations<String, Object, Object> user = redisTemplate.boundHashOps("user:"+phone);
                user.put(ip,map);
                //redisTemplate.opsForSet().add(ip,map);
                dataResult.setCode("200");
            }else {
                //同一个ip在60秒内超过三次不在发送验证码
                dataResult.setCode("201");
                dataResult.setMsg("操作过于频繁");

            }
        }

        return dataResult;
    }
}