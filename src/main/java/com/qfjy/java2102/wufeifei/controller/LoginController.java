package com.qfjy.java2102.wufeifei.controller;

import com.qfjy.java2102.wufeifei.common.DataResult;
import com.qfjy.java2102.wufeifei.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.thymeleaf.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


/**
 * @ClassName LoginController
 * @Description TODO
 * @Author wufeifei
 * @Date 2021/8/6
 * @Version 1.0
 */





@Controller
@RequestMapping("login")
public class LoginController {


    @Autowired
    private RedisTemplate<String,Object> redisTemplate;

    /**
     *   1    接收前端的手机号和密码，从数据库中查询手机号对应的密码，进行验证
     *   2    密码正确跳到主页，密码错误返回登录页面
     *   3    密码错误记录登录次数，如果2分钟内5次输入错误，则锁定账号，两小时内不允许登录

     */

    @RequestMapping("byPhone")
    @ResponseBody
    public DataResult loginByPhone(User user, HttpServletRequest request){


        //获取ip地址
        String ip = request.getRemoteAddr();
        //判断前端传入数据是否为空，为空则返回信息提示，重新输入
        if(StringUtils.isEmpty(user.getPhone())||StringUtils.isEmpty(user.getCode())||StringUtils.isEmpty(user.getPassword())){
             return DataResult.failureNull();
        }

        //判断手机号码对应的key是否存在
        if(!redisTemplate.hasKey(user.getPhone())){
            return DataResult.failureUsername();
        }


        //不为空则根据信息到数据库中查询密码和验证码进行比对
        HashMap<String, String> map= (HashMap<String, String>) redisTemplate.boundHashOps("user:" + user.getPhone()).get(ip);

        //获取对应的验证码进行比对，不对就返回提示信息
        for(Map.Entry<String, String> entry : map.entrySet()){
            int code=Integer.parseInt(entry.getKey());

            if (!String.valueOf(code).equals(user.getCode())){
                return DataResult.failureCode();
            }
        }
        //判断账号是否被锁定
        if(redisTemplate.hasKey("LockUser:"+user.getPhone())){
            Long expire = redisTemplate.getExpire("LockUser:" + user.getPhone());
            DataResult dataResult = new DataResult();
            dataResult.setCode("500");
            dataResult.setMsg("账号已被锁定，请"+expire+"秒后重试");
            return dataResult;
        }
        //获取密码
        String password = (String) redisTemplate.opsForValue().get(user.getPhone());
        if (password.equals(user.getPassword())){
            return DataResult.success();
        }else {
            //密码错误，记录次数，设定2分钟内最多错5次

            //判断是否两小时内首次登录
            if(redisTemplate.hasKey("LoginTimes:"+user.getPhone())){
                //不是首次判断是否达到五次
                String loginTimes = (String) redisTemplate.opsForValue().get("LoginTimes:" + user.getPhone());
                if(Integer.parseInt(loginTimes)>5){
                    redisTemplate.opsForValue().set("LockUser:"+user.getPhone(),"1");
                    redisTemplate.expire("LockUser:"+user.getPhone(),1, TimeUnit.HOURS);
                    return DataResult.failurePassword5Times();
                }else {
                    //两分钟内2-5次错误，每错一次，记录
                    String times= (String) redisTemplate.opsForValue().get("LoginTimes:"+user.getPhone());
                    int i = Integer.parseInt(times);
                    i+=1;
                    redisTemplate.opsForValue().set("LoginTimes:"+user.getPhone(),String.valueOf(i));
                    redisTemplate.expire("LoginTimes:"+user.getPhone(),2, TimeUnit.MINUTES);
                }
            }else{
                //首次登录创建次数记录，首次为1
                redisTemplate.opsForValue().set("LoginTimes:"+user.getPhone(),"1");
                redisTemplate.expire("LoginTimes:"+user.getPhone(),2, TimeUnit.MINUTES);
            }
            return DataResult.failure();
        }



    }











}
